package com.example.demo.config;

import com.example.demo.service.v2.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class StowEboosterDecoratorConfig {

    @Bean
    @Primary
    public StowingService myStowService() {
        return new StowingService();
    }

    @Bean
    @Qualifier("myStowServiceInitialPlacementDecorator")
    public StowingExternalServiceInitialPlacementDecorator stowServiceInitialPlacementDecorator(StowingService stowingService) {
        return new StowingExternalServiceInitialPlacementDecorator(stowingService);
    }

    @Bean
    @Qualifier("myStowServiceRelocationDecorator")
    public StowingExternalServiceRelocationDecorator stowServiceRelocationDecorator(StowingService stowingService) {
        return new StowingExternalServiceRelocationDecorator(stowingService);
    }

    @Bean
    @Qualifier("myStowServiceUnstowDecorator")
    public StowingExternalServiceUnstowDecorator stowServiceUnstowDecorator(StowingService stowingService) {
        return new StowingExternalServiceUnstowDecorator(stowingService);
    }

    @Bean
    @Qualifier("fullExternalServiceDecorator")
    public StowingExternalServiceDecorator fullExternalServiceDecorator() {
        return stowServiceRelocationDecorator(stowServiceInitialPlacementDecorator(stowServiceUnstowDecorator(myStowService())));
    }

}
