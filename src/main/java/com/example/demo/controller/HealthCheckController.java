package com.example.demo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * A spesial controller performing a healthcheck on the application.
 *
 * @author Simeon Markov
 */

@RestController
public class HealthCheckController {

    @GetMapping("/admin/healthcheck")
    public ResponseEntity<?> healthcheck() {
        return ResponseEntity.ok().build();
    }
}
