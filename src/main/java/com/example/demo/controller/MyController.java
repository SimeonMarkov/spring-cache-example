package com.example.demo.controller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.exception.ValidationException;
import com.example.demo.mapper.MyModelMapper;
import com.example.demo.model.AnotherModelDto;
import com.example.demo.model.CommonData;
import com.example.demo.model.MyModel;
import com.example.demo.model.MyModelDto;
import com.example.demo.service.MyService;
import com.example.demo.service.v2.StowingExternalServiceDecorator;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class MyController {

    private final MyService myService;
    private StowingExternalServiceDecorator fullDecorator;

    @Autowired
    public MyController(MyService myService, @Qualifier("fullExternalServiceDecorator") StowingExternalServiceDecorator fullDecorator) {
        this.myService = myService;
        this.fullDecorator = fullDecorator;
    }

    @GetMapping("/publishes")
    public String publico() {
        return "Public Page";
    }

    @GetMapping("/private")
    public String privata() {
        return "Private Page";
    }

    @GetMapping("/admin")
    public String admin() {
        return "Administrator Page";
    }

    @PreFilter("hasRole('USER')")
    @PostMapping("/save/my-model-dto")
    @Transactional
    public ResponseEntity createMyModels(@Valid @RequestBody MyModelDto data) throws ValidationException {
        return getResponseEntity(data);
    }

    @PostMapping("/save")
    public ResponseEntity createModels(@RequestBody List<MyModelDto> data) {
        data.stream().map(MyModelMapper.INSTANCE::myModelDtoToMyModel).collect(Collectors.toList());
        List<MyModel> myModel = data.stream().map(MyModelMapper.INSTANCE::myModelDtoToMyModel).collect(Collectors.toList());
        return ResponseEntity.ok(myService.saveAll(myModel));
    }

    @Cacheable(value = "models", condition = "#data.modelType == 'type'")
    @PostMapping("/search")
    public Page<MyModel> getModelsBy(@Valid @RequestBody MyModelDto data, @RequestParam("page") int page, @RequestParam("size") int size) {
        MyModel myModel = MyModelMapper.INSTANCE.myModelDtoToMyModel(data);
        Example<MyModel> criteria = Example.of(myModel, ExampleMatcher.matchingAny());
        return myService.findAll(criteria, PageRequest.of(page, size, Sort.Direction.ASC, "id"));
    }

    @PostMapping("/save/another-model-dto")
    public ResponseEntity createAnotherModels(@Valid @RequestBody AnotherModelDto data) throws ValidationException {
        return getResponseEntity(data);
    }

    @PostMapping(value = "/import")
    public ResponseEntity createModelsFromCsv(@RequestParam("file") MultipartFile file) {
        try {
            myService.calculateDelta(file);
            return ResponseEntity.ok().build();
        } catch (RuntimeException | IOException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    private ResponseEntity getResponseEntity(CommonData data) throws ValidationException {
        MyModel myModel = MyModelMapper.INSTANCE.convert(data);
        return ResponseEntity.ok(myService.save(myModel));
    }
}
