package com.example.demo.service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.model.MyModel;
import com.example.demo.reader.MyModelReader;
import com.example.demo.repository.MyRepository;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class MyService {

    private static final String CRON_EXPRESSION = "* 37/10 * * * *";
    private final MyRepository myRepository;
    private final CronSequenceGenerator cronTrigger = new CronSequenceGenerator(CRON_EXPRESSION);

    @Autowired
    public MyService(MyRepository repository) {
        this.myRepository = repository;
    }

    @Scheduled(cron = CRON_EXPRESSION)
    public void scheduledLog() {
        log.info("Execution time:{}. Next point of execution:{}", new Date(), cronTrigger.next(new Date()));
    }

    public Page<MyModel> findAll(Example<MyModel> example, Pageable page) {
        return myRepository.findAll(example, page);
    }

    public MyModel save(MyModel myModel) {
        return myRepository.save(myModel);
    }

    public List<MyModel> saveAll(List<MyModel> models) {
        return myRepository.saveAll(models);
    }

    private void addToCsv(List<MyModel> models) {
        try {
            CsvMapper mapper = new CsvMapper();
            CsvSchema schema = mapper.schemaFor(MyModel.class);
            ObjectWriter myObjectWriter = mapper.writer(schema);
            File modelsCsv = new File("src/main/resources/models.csv");
            FileOutputStream tempFileOutputStream = new FileOutputStream(modelsCsv, true);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(tempFileOutputStream, 1024);
            OutputStreamWriter writerOutputStream = new OutputStreamWriter(bufferedOutputStream, StandardCharsets.UTF_8);
            myObjectWriter.writeValue(writerOutputStream, models);
        } catch (IOException e) {
            log.error("Couldn't write to csv file...");
        }
    }

    private File validateFile(MultipartFile file) throws IOException {
        Files.write(Paths.get("src/main/resources/models-temp.csv"), file.getBytes());

        return new File("src/main/resources/models-temp.csv");
    }

    @Transactional
    public void calculateDelta(MultipartFile newFile) throws IOException {
        File newFileTemp = validateFile(newFile);
        HashSet<String> newCsv = new HashSet<>(Files.readAllLines(Paths.get(newFileTemp.getPath())));
        HashSet<String> currentCsv = new HashSet<>(Files.readAllLines(Paths.get("src/main/resources/models.csv")));
        HashSet<String> temp = new HashSet<>(currentCsv);
        temp.removeAll(newCsv);
        //save newly added locations to db
        deleteAllOutdatedLocations(temp);
        temp = new HashSet<>(newCsv);
        temp.removeAll(currentCsv);
        //delete removed locations from db
        saveAllNewLocations(temp);

        updateCsv(currentCsv, newCsv, temp);

        //delete temp csv
        Files.deleteIfExists(Paths.get("src/main/resources/models-temp.csv"));
    }

    private void updateCsv(HashSet<String> currentCsv, HashSet<String> newCsv, HashSet<String> temp) throws IOException {
        HashSet<String> mergedCsvs = new HashSet<>(temp);
        temp = new HashSet<>(newCsv);
        temp.retainAll(currentCsv);
        mergedCsvs.addAll(temp);
        temp = new HashSet<>(newCsv);
        temp.removeAll(currentCsv);
        mergedCsvs.addAll(temp);

        //append new and remove deleted locations from csv
        Files.write(Paths.get("src/main/resources/models.csv"), mergedCsvs, StandardOpenOption.TRUNCATE_EXISTING);
    }


    private void saveAllNewLocations(HashSet<String> finalTemp1) {
        List<MyModel> newModels = MyModelReader.convert(finalTemp1);
        myRepository.saveAll(newModels);
    }

    private void deleteAllOutdatedLocations(HashSet<String> finalTemp) {
        List<MyModel> newModels = MyModelReader.convert(finalTemp);
        myRepository.deleteAll(newModels);
    }
}
