package com.example.demo.service.v2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StowingExternalServiceRelocationDecorator extends StowingExternalServiceDecorator {

    private StowingService stowingService;

    @Autowired
    private ExternalUpdater externalUpdater;

    public StowingExternalServiceRelocationDecorator(StowingService stowingService) {
        this.stowingService = stowingService;
    }

    @Override
    public void stowInitPlacement() {
        stowingService.stowInitPlacement();
    }

    @Override
    public void stowRelocate() {
        stowingService.stowRelocate();
        System.out.println("Decorating relocate...");
        externalUpdater.eboosterRelocate();
    }

    @Override
    public void stowUnstow() {
        stowingService.stowUnstow();
    }
}
