package com.example.demo.service.v2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StowingExternalServiceUnstowDecorator extends StowingExternalServiceDecorator {

    private StowingService stowingService;

    @Autowired
    private ExternalUpdater externalUpdater;

    public StowingExternalServiceUnstowDecorator(StowingService stowingService) {
        this.stowingService = stowingService;
    }

    @Override
    public void stowInitPlacement() {
        stowingService.stowInitPlacement();
    }

    @Override
    public void stowRelocate() {
        stowingService.stowRelocate();
    }

    @Override
    public void stowUnstow() {
        stowingService.stowUnstow();
        System.out.println("Decorating unstow...");
        externalUpdater.eboosterUnstow();
    }
}
