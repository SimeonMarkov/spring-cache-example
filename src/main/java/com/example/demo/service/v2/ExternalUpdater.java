package com.example.demo.service.v2;

import org.springframework.stereotype.Component;

@Component
public class ExternalUpdater {

    void eboosterInitPlacement() {
        System.out.println("Initial placement ebooster...");
    }

    void eboosterRelocate() {
        System.out.println("Relocate ebooster");
    }

    void eboosterUnstow() {
        System.out.println("Unstow ebooster");
    }
}
