package com.example.demo.service.v2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StowingExternalServiceInitialPlacementDecorator extends StowingExternalServiceDecorator {

    private StowingService stowingService;

    @Autowired
    private ExternalUpdater externalUpdater;

    public StowingExternalServiceInitialPlacementDecorator(StowingService stowingService) {
        this.stowingService = stowingService;
    }

    @Override
    public void stowInitPlacement() {
        stowingService.stowInitPlacement();
        System.out.println("Decorating initial placement...");
        externalUpdater.eboosterInitPlacement();
    }

    @Override
    public void stowRelocate() {
        stowingService.stowRelocate();
    }

    @Override
    public void stowUnstow() {
        stowingService.stowUnstow();
    }
}
