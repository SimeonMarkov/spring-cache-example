package com.example.demo.service.v2;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("myStowService")
public class StowingService {

    public StowingService() {
    }

    public void stowInitPlacement() {
        System.out.println("Init placement stow...");
    }

    public void stowRelocate() {
        System.out.println("Relocate stow...");
    }

    public void stowUnstow() {
        System.out.println("Unstow stow...");
    }
}
