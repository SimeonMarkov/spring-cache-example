package com.example.demo.service;

import com.example.demo.model.Auth;
import com.example.demo.model.OperationType;
import com.example.demo.model.Root;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class ScheduledJobsService {

    private final RestTemplate restTemplate;

    @Autowired
    public ScheduledJobsService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public void postRoot() {
        asyncMethod();

        Root root = Root.builder()
                .auth(
                        Auth.builder()
                                .accessKey("access123")
                                .userId("user123").build())
                .operationType(
                        OperationType.builder()
                                .warehouseId("warehouse123")
                                .articleId("articleId123").build())
                .build();
        restTemplate.postForEntity("http://localhost:8080/my-controller/ebooster", root, Root.class);

        log.info("RESULT...");
    }

    @Async
    void asyncMethod() {
        for (int i = 1; i <= 10; i++) {
            log.info("RESULT: {}", i);
        }
    }
}
