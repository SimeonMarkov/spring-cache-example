package com.example.demo.health;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;

import static org.springframework.boot.actuate.health.Status.DOWN;

public abstract class AbstractHealthIndicator implements HealthIndicator {
    @Override
    public Health health() {
        Health health = this.healthcheck();
        if (DOWN.equals(health.getStatus())) {
            return Health.down().build();
        }
        return Health.up().build();
    }

    abstract Health healthcheck();
}
