package com.example.demo.health;

import com.example.demo.repository.MyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DatabaseHealthIndicator extends AbstractHealthIndicator {

    private final MyRepository repository;

    @Autowired
    public DatabaseHealthIndicator(MyRepository repository) {
        this.repository = repository;
    }

    @Override
    Health healthcheck() {
        try {
            repository.count();
        } catch (Exception e) {
            log.error("Database health failed:", e);
            return Health.down().withDetail("db", "Database health failed").build();
        }

        return Health.up().build();
    }
}
