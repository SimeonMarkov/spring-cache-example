package com.example.demo.reader;

import com.example.demo.model.MyModel;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class MyModelReader {

    public static List<MyModel> convert(Collection<String> data) {
        List<MyModel> result = new ArrayList<>();
        data.forEach(row -> {
            StringReader reader = new StringReader(row);
            CsvMapper m = new CsvMapper();
            CsvSchema schema = m.schemaFor(MyModel.class).withoutHeader().withLineSeparator("\n").withColumnSeparator(',');
            try {
                MappingIterator<MyModel> r = m.readerWithSchemaFor(MyModel.class).with(schema).readValues(reader);
                while (r.hasNext()) {
                    result.add(r.nextValue());
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        return result;
    }
}
