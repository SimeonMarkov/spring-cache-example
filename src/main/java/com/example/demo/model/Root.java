package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Root {

    @JsonProperty
    private Auth auth;
    @JsonProperty
    private OperationType operationType;
}
