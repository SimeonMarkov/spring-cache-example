package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
public class AnotherModelDto extends CommonData {

    private String nan;

    @Builder
    @JsonCreator
    public AnotherModelDto(@JsonProperty(value = "modelName") String modelName, @JsonProperty(value = "modelType") String modelType, @JsonProperty(value = "nan") String nan) {
        super(modelName, modelType);
        this.nan = nan;
    }
}
