package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "my_model")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MyModel {

    @Id
    @GeneratedValue(generator = "uuid2-gen")
    @GenericGenerator(name = "uuid2-gen", strategy = "uuid2")
    private String id;

    private String name;
    private String type;

    private LocalDateTime time;

    private String gtin;
    private String nan;
}
