package com.example.demo.model;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.PropertyAccessorFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

public class EqualFieldsValidator implements ConstraintValidator<EqualFields, Object> {

    private String firstField;
    private String secondField;

    @Override
    public void initialize(EqualFields constraintAnnotation) {
        this.firstField = constraintAnnotation.first();
        this.secondField = constraintAnnotation.second();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        try {
            BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(value);
            final Object firstObj = wrapper.getPropertyValue(firstField);
            final Object secondObj = wrapper.getPropertyValue(secondField);

            return Objects.requireNonNull(firstObj).equals(secondObj);
        } catch (final Exception ignore) {
            // ignore
        }
        return false;
    }
}
