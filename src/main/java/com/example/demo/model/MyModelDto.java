package com.example.demo.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Pattern;

@Data
@MultipleEqualFields({
        @EqualFields(first = "gtin", second = "gtinBackup"),
        @EqualFields(first = "gtinBackup2", second = "gtinBackup3"),
})
@AllArgsConstructor

public class MyModelDto extends CommonData {

    @Pattern(regexp = "gtin[1-9][0-9]*")
    private String gtin;

    private String gtinBackup;
    private int gtinBackup2;
    private int gtinBackup3;

    @Builder
    @JsonCreator
    public MyModelDto(
            @JsonProperty(value = "modelName") String modelName,
            @JsonProperty(value = "modelType") String modelType,
            @JsonProperty(value = "gtin") String gtin,
            @JsonProperty(value = "gtinBackup") String gtinBackup,
            @JsonProperty(value = "gtinBackup2") int gtinBackup2,
            @JsonProperty(value = "gtinBackup3") int gtinBackup3) {
        super(modelName, modelType);
        this.gtin = gtin;
        this.gtinBackup = gtinBackup;
        this.gtinBackup2 = gtinBackup2;
        this.gtinBackup3 = gtinBackup3;
    }
}
