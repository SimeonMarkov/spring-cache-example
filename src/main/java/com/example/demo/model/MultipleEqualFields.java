package com.example.demo.model;

import java.lang.annotation.*;

@Documented
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface MultipleEqualFields {

    EqualFields[] value() default {};
}
