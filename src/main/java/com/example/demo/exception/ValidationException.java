package com.example.demo.exception;

public class ValidationException extends Exception {

    public ValidationException(String msg) {
        super(msg);
    }
}
