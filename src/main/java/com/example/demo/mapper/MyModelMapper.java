package com.example.demo.mapper;

import com.example.demo.model.AnotherModelDto;
import com.example.demo.model.CommonData;
import com.example.demo.model.MyModel;
import com.example.demo.model.MyModelDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import java.time.LocalDateTime;
import java.util.UUID;

@Mapper(imports = {LocalDateTime.class, UUID.class})
public abstract class MyModelMapper {

    public static final MyModelMapper INSTANCE = Mappers.getMapper(MyModelMapper.class);

    @Mappings({
            @Mapping(target = "id", expression = "java(UUID.randomUUID().toString())"),
            @Mapping(source = "modelName", target = "name"),
            @Mapping(source = "modelType", target = "type", defaultValue = "SAME_TYPE"),
            @Mapping(target = "time", expression = "java(LocalDateTime.now())")

    })
    public  abstract MyModel myModelDtoToMyModel(MyModelDto myModelDto);

    @Mappings({
            @Mapping(target = "id", expression = "java(UUID.randomUUID().toString())"),
            @Mapping(source = "modelName", target = "name"),
            @Mapping(source = "modelType", target = "type", defaultValue = "SAME_TYPE"),
            @Mapping(target = "time", expression = "java(LocalDateTime.now())")

    })
    protected abstract MyModel anotherModelDtoToAnotherModel(AnotherModelDto myModelDto);

    public MyModel convert(CommonData commonData) {
        if (commonData instanceof MyModelDto) {
            return myModelDtoToMyModel((MyModelDto) commonData);
        } else {
            return anotherModelDtoToAnotherModel((AnotherModelDto) commonData);
        }
    }
}
